import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormFiscaliaComponent } from './form-fiscalia.component';

describe('FormFiscaliaComponent', () => {
  let component: FormFiscaliaComponent;
  let fixture: ComponentFixture<FormFiscaliaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormFiscaliaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormFiscaliaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
