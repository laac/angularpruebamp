import { Component, OnInit, OnDestroy} from '@angular/core';
import { ApiService } from '../api.service';
import { FormBuilder, Validators } from '@angular/forms';
import {Router} from "@angular/router";
import {ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-form-fiscalia',
  templateUrl: './form-fiscalia.component.html',
  styleUrls: ['./form-fiscalia.component.css']
})
export class FormFiscaliaComponent implements OnInit {

  private req:any;
  departamentos: [any];
  private regForm:any;
  private id:number;

  constructor(private route: ActivatedRoute,private apiService: ApiService, private formBuilder: FormBuilder, private router: Router) { 

  }

  ngOnInit() {
  	this.route.params.subscribe(params => {
  		console.log(params);
  		this.id = params['id'];
  	})
	this.req = this.apiService.getDepartamentos().subscribe((data)=>{
      this.departamentos = data as any;
    });
    this.regForm=this.formBuilder.group({
          nombre:['', Validators.required],
          direccion:['', Validators.required],
          telefonos:['', Validators.required],
          id_departamento:['', Validators.required]
    })
  }

  ngOnDestroy(){
  	this.req.unsubscribe();
  }

  onSubmit() {
  	console.log(this.id == undefined);
    if (this.regForm.dirty && this.regForm.valid) {
    	
    	if(this.id == undefined){
    		this.apiService.createFiscalia(
    		this.regForm.value.nombre,
    		this.regForm.value.telefonos,
    		this.regForm.value.direccion,
    		this.regForm.value.id_departamento
    		).subscribe((data)=>{this.router.navigate(['/lista-fiscalias'])});
    	}else{
    		this.apiService.editFiscalia(
    		this.id,
    		this.regForm.value.nombre,
    		this.regForm.value.telefonos,
    		this.regForm.value.direccion,
    		this.regForm.value.id_departamento
    		).subscribe((data)=>{
      		this.router.navigate(['/lista-fiscalias'])});
    	}
    }
  }

}