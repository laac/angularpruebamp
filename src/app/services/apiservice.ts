import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'


@Injectable({
  providedIn: 'root'
})
export class apiservice {
	constructor(private httpclient: HttpClient) { }

	getDepartamentos(): Observable<any>{
		return this.httpclient.get('http://localhost:8080/PruebaApiMP/webresources/departamento/getDepartamentos')
	}

	deleteFiscalia(id:number): Observable<any>{
		return this.httpclient.post('http://localhost:8080/PruebaApiMP/webresources/fiscalia/remove',{idFiscalia: id});
	}

}