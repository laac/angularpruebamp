import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { ListaFiscaliasComponent } from './lista-fiscalias/lista-fiscalias.component';
import { HomeComponent } from './home/home.component';
import { FormFiscaliaComponent } from './form-fiscalia/form-fiscalia.component';

import { HttpClientModule } from '@angular/common/http';
import { DeleteFiscaliaComponent } from './delete-fiscalia/delete-fiscalia.component';

@NgModule({
  declarations: [
    AppComponent,
    ListaFiscaliasComponent,
    HomeComponent,
    FormFiscaliaComponent,
    DeleteFiscaliaComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BsDropdownModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
