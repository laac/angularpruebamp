import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaFiscaliasComponent } from './lista-fiscalias.component';

describe('ListaFiscaliasComponent', () => {
  let component: ListaFiscaliasComponent;
  let fixture: ComponentFixture<ListaFiscaliasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaFiscaliasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaFiscaliasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
