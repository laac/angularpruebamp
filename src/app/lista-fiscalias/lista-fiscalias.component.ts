import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-lista-fiscalias',
  templateUrl: './lista-fiscalias.component.html',
  styleUrls: ['./lista-fiscalias.component.css']
})

export class ListaFiscaliasComponent implements OnInit, OnDestroy {

  private req:any;
  items: [any];

  constructor(private apiService: ApiService) { 

  }

  ngOnInit() {
	this.req = this.apiService.getFiscalias().subscribe((data)=>{
      this.items = data as any;
    });
    console.log(this.items);
  }

  ngOnDestroy(){
  	this.req.unsubscribe();
  }

}
