import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import { ListaFiscaliasComponent } from './lista-fiscalias/lista-fiscalias.component';

const appRoutes: Routes = [
	{
		path: "lista-fiscalias",
		component: ListaFiscaliasComponent,
	}
]

@NgModule({
		imports: [
			RouterModule.forRoot(appRoutes)
		],
		exports: [
			RouterModule
		]
})
export class AppRoutingModule {}