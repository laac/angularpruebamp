import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaFiscaliasComponent } from './lista-fiscalias/lista-fiscalias.component';
import { HomeComponent } from './home/home.component';
import { FormFiscaliaComponent } from './form-fiscalia/form-fiscalia.component';
import { DeleteFiscaliaComponent } from './delete-fiscalia/delete-fiscalia.component';


const routes: Routes = [
	{
		path: "",
		component: HomeComponent,
	},
	{
		path: "lista-fiscalias",
		component: ListaFiscaliasComponent,
	},
	{
		path: "agregar-fiscalias",
		component: FormFiscaliaComponent,
	},
	{
		path: "fiscalia/remove/:id",
		component: DeleteFiscaliaComponent,
	},
	{
		path: "fiscalia/edit/:id",
		component: FormFiscaliaComponent,
	},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
