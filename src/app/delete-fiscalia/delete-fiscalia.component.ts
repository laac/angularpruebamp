import { Component, OnInit } from '@angular/core';
import {ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import {Router} from "@angular/router"

@Component({
  selector: 'app-delete-fiscalia',
  templateUrl: './delete-fiscalia.component.html',
  styleUrls: ['./delete-fiscalia.component.css']
})
export class DeleteFiscaliaComponent implements OnInit {

  constructor(private route: ActivatedRoute, private apiService: ApiService, private router: Router) { }

  private id:number;

  ngOnInit() {
  	this.route.params.subscribe(params => {
  		this.apiService.deleteFiscalia(params['id']).subscribe((data)=>{
      		this.router.navigate(['/lista-fiscalias'])
    	});
  	})
  }

}
