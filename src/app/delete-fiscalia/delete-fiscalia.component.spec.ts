import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteFiscaliaComponent } from './delete-fiscalia.component';

describe('DeleteFiscaliaComponent', () => {
  let component: DeleteFiscaliaComponent;
  let fixture: ComponentFixture<DeleteFiscaliaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteFiscaliaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteFiscaliaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
