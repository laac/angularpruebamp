import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) { }
    public getDepartamentos(){
    return this.httpClient.get(`http://localhost:8080/PruebaApiMP/webresources/departamento/getDepartamentos`);
  }

  public getFiscalias(){
    return this.httpClient.get(`http://localhost:8080/PruebaApiMP/webresources/fiscalia`);
  }

  public deleteFiscalia(id: number){
		return this.httpClient.post('http://localhost:8080/PruebaApiMP/webresources/fiscalia/remove',{idFiscalia: id});
	}

public createFiscalia(Nombre: string, Telefonos: string, Direccion: string, Id_departamento: number){
		return this.httpClient.post('http://localhost:8080/PruebaApiMP/webresources/fiscalia',
			{
				nombre: Nombre,
				telefonos: Telefonos,
				direccion: Direccion,
				idDepartamento: {
					idDepartamento: Id_departamento
				}
			});
	}

public editFiscalia(id:number,Nombre: string, Telefonos: string, Direccion: string, Id_departamento: number){
		return this.httpClient.post('http://localhost:8080/PruebaApiMP/webresources/fiscalia/edit',
			{
				idFiscalia: id,
				nombre: Nombre,
				telefonos: Telefonos,
				direccion: Direccion,
				idDepartamento: {
					idDepartamento: Id_departamento
				}
			});
	}
}